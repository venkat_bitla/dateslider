(function() {
  var TimeSlider = function(element, options) {
      _this = this;
      this.element = element;
      this.options = options != null ? options : {};
      this.tooltip = d3.select("body").append("div").attr("class", "tooltip").style("opacity", 0);
      this.svg = d3.select(element).append('svg').attr('class', 'timeslider');
      this.timeouts = [];

      var debounce = function(timeout, id, fn) {
        if (!(timeout && id && fn)) {
          return;
        }
        
        if (!_this.timeouts[id]) {
          _this.timeouts[id] = -1;
        }
        
        return function() {
          if (_this.timeouts[id] > -1) {
            window.clearTimeout(_this.timeouts[id]);
          }

          return _this.timeouts[id] = window.setTimeout(fn, timeout);
        };
      };

      var customFormatter = function(formats) {
        return function(date) {
          var i = formats.length - 1;
          var f = formats[i];
          while (!f[1](date)) {
            f = formats[i--];
          }
          return f[0](date);
        };
      };


      var customFormats = customFormatter([
        [
          d3.time.format("%Y"), function() {
            return true;
          }
        ], [
          d3.time.format("%B %Y"), function(d) {
            return d.getUTCMonth();
          }
        ], [
          d3.time.format("%b %d %Y"), function(d) {
            return d.getUTCDate() !== 1;
          }
        ], [
          d3.time.format("%b %d %Y "), function(d) {
            return d.getUTCDay() && d.getUTCDate() !== 1;
          }
        ], [
          d3.time.format("%I %p"), function(d) {
            return d.getUTCHours();
          }
        ], [
          d3.time.format("%I:%M"), function(d) {
            return d.getUTCMinutes();
          }
        ], [
          d3.time.format(":%S"), function(d) {
            return d.getUTCSeconds();
          }
        ], [
          d3.time.format(".%L"), function(d) {
            return d.getUTCMilliseconds();
          }
        ]
      ]);

      this.scales = {
        x: d3.time.scale.utc().domain([this.options.domain.start, this.options.domain.end]).range([0, this.options.width]).nice()
      };

      this.axis = {
        x: d3.svg.axis().scale(this.scales.x).innerTickSize(this.options.height - 13).tickFormat(customFormats)
      };
      
      this.svg.append('g').attr('class', 'axis').call(this.axis.x);
      d3.select(this.element).select('g.axis .domain').attr('transform', "translate(0, " + (options.height - 18) + ")");
      
      this.brushStart = function() {
        _this.options.lastZoom = {
          scale: _this.options.zoom.scale(),
          translate: [_this.options.zoom.translate()[0], _this.options.zoom.translate()[1]]
        };
        return _this.options.zoom.on('zoom', null);
      }

      this.brushEnd = function() {
        _this.options.zoom.scale(_this.options.lastZoom.scale).translate(_this.options.lastZoom.translate).on('zoom', zoom);
        return _this.element.dispatchEvent(new CustomEvent('selectionChanged', {
          detail: {
            start: _this.brush.extent()[0],
            end: _this.brush.extent()[1]
          },
          bubbles: true,
          cancelable: true
        }));
      }
      
      this.brush = d3.svg.brush().x(this.scales.x)
        .on('brushstart', this.brushStart)
        .on('brushend', this.brushEnd)
        .extent([this.options.brush.start, this.options.brush.end]);
      
      this.svg.append('g').attr('class', 'brush')
        .call(this.brush).selectAll('rect')
        .attr('height', "" + (this.options.height - 18))
        .attr('y', 0);

      this.redraw = function() {
        _this.brush.x(_this.scales.x).extent(_this.brush.extent());
        d3.select(_this.element).select('g.axis').call(_this.axis.x);
        d3.select(_this.element).select('g.brush').call(_this.brush);
      };

      var resize = function() {
        var svg = d3.select(_this.element).select('svg.timeslider')[0][0];
        _this.scales.x.range([0, _this.options.width]);
        return _this.redraw();
      };
     
      d3.select(window).on('resize', resize);
     
      var zoom = function() {
        return _this.redraw();
      };
      
      this.options.zoom = d3.behavior.zoom().x(this.scales.x).size([this.options.width, this.options.height]).scaleExtent([1, Infinity]).on('zoom', zoom);
      this.svg.call(this.options.zoom);
  }

  TimeSlider.prototype.zoom = function() {
    var _this = this;
    var _ref = "";
    var params = 1 <= arguments.length ? [].slice.call(arguments, 0) : [];
    var start = new Date(params[0]);
    var end = new Date(params[1]);
    
    if (end < start) {
      _ref = [end, start], 
      start = _ref[0], 
      end = _ref[1];
    }
    
    d3.transition().duration(750).tween('zoom', function() {
      var iScale = d3.interpolate(_this.options.zoom.scale(), (_this.options.domain.end - _this.options.domain.start) / (end - start));
      return function(t) {
        var iPan = d3.interpolate(_this.options.zoom.translate()[0], _this.options.zoom.translate()[0] - _this.scales.x(start));
        _this.options.zoom.scale(iScale(t));
        _this.options.zoom.translate([iPan(t), 0]);
        return _this.redraw();
      };
    });
    
    return true;
  
  }; 

  TimeSlider.prototype.reset = function() {
    this.zoom(this.options.domain.start, this.options.domain.end);
    return true;
  };

  TimeSlider.prototype.select = function() {
    var _ref;
    var params = 1 <= arguments.length ? [].slice.call(arguments, 0) : [];
    if (params.length !== 2) {
      return false;
    }
    var start = new Date(params[0]);
    var end = new Date(params[1]);
      
    if (end < start) {
      _ref = [end, start], start = _ref[0], end = _ref[1];
    }
    
    if (start < this.options.start) {
      start = this.options.start;
    }
    
    if (end > this.options.end) {
      end = this.options.end;
    }
    
    d3.select(this.element).select('g.brush').call(this.brush.extent([start, end]));
      this.element.dispatchEvent(new CustomEvent('selectionChanged', {
        detail: {
          start: this.brush.extent()[0],
          end: this.brush.extent()[1]
        },
        bubbles: true,
        cancelable: true
      }));

    return true;
  
  };  

  this.TimeSlider = TimeSlider;

}).call(this);
